import './App.css';
import FullName from './components/FullName';
import Address from './components/Address';
import ProfilePhoto from './components/ProfilePhoto';
import Tilt from 'react-parallax-tilt';

function App() {
  return (
    <div className="App">
      <Tilt>
      <div className="content first-content">
          <ProfilePhoto></ProfilePhoto>
          <FullName/>
          <Address></Address>
      </div>
        </Tilt>
    </div>
  );
}

export default App;
