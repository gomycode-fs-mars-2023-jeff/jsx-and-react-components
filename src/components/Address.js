import React from 'react';

const Address = () => {
    const addressStyle = {
        fontWeight: '200',
    }
    return (
        <div>
            <h5>Abidjan, Côte d'Ivoire</h5>
            <p style={addressStyle}>Developer FullStack JS Junior from Cocody</p>
        </div>
    );
}

export default Address;
