import React from 'react';

const ProfilePhoto = () => {
    return (
        <div>
            <img src='/pic1.jpg' alt="" style={{
                width: '200px', 
                clipPath: 'circle()' 
            }}/>
        </div>
    );
}

export default ProfilePhoto;
